# dusiburg

Dusiburg government website.

## How to update the site ?

1. Make changes
2. Commit & Push
3. Wait for pipeline to finish
4. You can see you change here : https://dusworld.gitlab.io/dusiburg

## Technologies used

- HTML + CSS + JS with Purecss.io
- Gitlab pipeline using https://jekyllrb.com/ to deploy static assets (HTML + CSS + JS)